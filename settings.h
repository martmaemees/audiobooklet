#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDataStream>

struct Settings {
public:
    int volumeLevel = 100;

    static Settings load();
    void save();
};

QDataStream& operator<<(QDataStream& out, const Settings& settings);
QDataStream& operator>>(QDataStream& in, Settings& settings);

#endif // SETTINGS_H
