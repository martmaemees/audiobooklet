#include "book.h"

QDataStream& operator<<(QDataStream& out, const Book& book)
{
    out << book.title << book.author << quint64(book.totalLength) << book.chapters << book.currentChapter << book.currentPosition;
    return out;
}

QDataStream& operator>>(QDataStream& in, Book& book)
{
    quint64 length;
    book = Book();
    in >> book.title >> book.author >> length >> book.chapters >> book.currentChapter >> book.currentPosition;
    book.totalLength = length;
    return in;
}

QDataStream& operator<<(QDataStream& out, const Chapter& chapter)
{
    out << chapter.title << chapter.order << chapter.length << chapter.fileName;
    return out;
}

QDataStream& operator>>(QDataStream& in, Chapter& chapter)
{
    chapter = Chapter();
    in >> chapter.title >> chapter.order >> chapter.length >> chapter.fileName;
    return in;
}
