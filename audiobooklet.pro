QT += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#TARGET = audiobooklet
#TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++11

HEADERS = settings.h util.h book.h mainwindow.h
SOURCES = main.cpp settings.cpp util.cpp book.cpp mainwindow.cpp
FORMS += mainwindow.ui
#LIBS += -ltag -ltag_c
#CONFIG += staticlib
#win32:LIBS += -L"$$_PRO_FILE_PWD_/winlib"
#LIBS += -L"$$_PRO_FILE_PWD_/winlib"
#INCLUDEPATH += "$$_PRO_FILE_PWD_/include"


qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

LIBS += -ltag
win32: LIBS += -L$$PWD/winlib/

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/winlib/tag.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/winlib/libtag.a
win32: DEFINES += TAGLIB_STATIC
