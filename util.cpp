#include "util.h"
#include <QString>
#include <QStandardPaths>

QDir getAppDataDir()
{
   QString appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
   QDir appDataDir(appDataPath);
   if (!appDataDir.exists()) {
       appDataDir.mkpath(".");
   }
   return appDataDir;
}
