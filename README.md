# Audiobooklet

Audiobooklet is a simple program that allows adding audiobooks to a library and listening to them. It allows books divided intomultiple audiofiles, continuing to the next file (chapter) as the end of the current one is reached.

The library contents and the progress made in each book is saved into a file on adding a new book and closing the program. This allows the user to start at the exact spot they were at on application restart.

## Requirements

* Qt 5.11+
* Taglib

## Compilation

### Taglib

The project uses the TagLib library to read the tags from music files.

#### Linux

On most Linux distributions the taglib library can usually be installed through the package manager:

**Arch Linux**
```
pacman -S taglib
```

**Ubuntu**
```
apt install libtag1-dev
```

Alternatively, the source can be downloaded from [GitHub](https://github.com/taglib/taglib) and installed using the [Installation instructions](https://github.com/taglib/taglib/blob/master/INSTALL.md).

#### Windows

The repository contains built static library files for the Taglib library in the winlib directory. The qmake file should detect the win32 platform and use the libs from the included directory. This was tested with MSVC 2015 x64 and MSVC 2017 x64 compilers.

If the compiled version does not work, the library can be compiled manually, following the [Installation instructions](https://github.com/taglib/taglib/blob/master/INSTALL.md)

#### MacOS

I don't have access to a MacOS installation to test installation, but [TagLib Installation Instructions](https://github.com/taglib/taglib/blob/master/INSTALL.md) include MacOS.

### Application

For building the application, open the qmake project in QtCreator (Qt 5.11+) and build it.

Tested with Qt Creator 4.12.0 and Qt 5.14.2.

