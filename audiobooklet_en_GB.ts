<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="audiobooklet_en_GB">
<context>
    <name>BookListItem</name>
    <message>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_booklistitem.h" line="67"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_booklistitem.h" line="68"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_mainwindow.h" line="278"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_mainwindow.h" line="279"/>
        <source>Books</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="88"/>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_mainwindow.h" line="280"/>
        <source>Add Book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="110"/>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_mainwindow.h" line="281"/>
        <source>Remove Book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="155"/>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_mainwindow.h" line="283"/>
        <source>Author:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="192"/>
        <location filename="mainwindow.ui" line="217"/>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_mainwindow.h" line="285"/>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_mainwindow.h" line="286"/>
        <source>00:00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="242"/>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_mainwindow.h" line="287"/>
        <source>Last Chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="257"/>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_mainwindow.h" line="288"/>
        <source>Back 10s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="272"/>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_mainwindow.h" line="289"/>
        <location filename="mainwindow.cpp" line="222"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="287"/>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_mainwindow.h" line="290"/>
        <source>Forward 10s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="302"/>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_mainwindow.h" line="291"/>
        <source>Next Chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="321"/>
        <location filename="build-audiobooklet-Desktop-Debug/audiobooklet_autogen/include/ui_mainwindow.h" line="292"/>
        <source>Volume:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="59"/>
        <source>Choose audio file(s)</source>
        <oldsource>Audio Files (*.mp4, *.m4b)</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="59"/>
        <source>Audio Files (*.mp4 *.m4b *.mp3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="104"/>
        <location filename="mainwindow.cpp" line="119"/>
        <source>Unable to open the library file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="214"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <source>Are you sure you want to remove %1 from the library?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
