#ifndef BOOK_H
#define BOOK_H

#include <QList>
#include <QString>
#include <QDataStream>

struct Chapter {
    QString title;
    unsigned int length;
    unsigned int order;
    QString fileName;
};

struct Book {
    QString title;
    QString author;
    unsigned long totalLength;

    QList<Chapter> chapters;

    int currentChapter = 0;
    int currentPosition = 0;

    const Chapter& activeChapter() const {
        return chapters.at(currentChapter);
    }
};

QDataStream& operator<<(QDataStream& out, const Book& book);
QDataStream& operator>>(QDataStream& in, Book& book);
QDataStream& operator<<(QDataStream& out, const Chapter& chapter);
QDataStream& operator>>(QDataStream& in, Chapter& chapter);

#endif // BOOK_H
