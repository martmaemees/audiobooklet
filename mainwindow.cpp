#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <taglib/tag.h>
#include <taglib/fileref.h>

#include <QFileDialog>
#include <QDataStream>
#include <QStandardPaths>
#include <QDebug>
#include <QMessageBox>
#include <iostream>
#include <algorithm>
#include "util.h"

static const QString libraryFileName = "library.bin";

QString millisecondsToTimespan(int ms) {
    int seconds = ms / 1000;
    int minutes = seconds / 60;
    if (minutes <= 99) {
        return QString("%1:%2").arg(minutes, 2, 10, QLatin1Char('0')).arg(seconds % 60, 2, 10, QLatin1Char('0'));
    } else {
        return QString("%1:%2:%3").arg(minutes / 60).arg(minutes % 60, 2, 10, QLatin1Char('0')).arg(seconds % 60, 2, 10, QLatin1Char('0'));
    }
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    settings = Settings::load();
    player->setVolume(settings.volumeLevel);
    ui->volumeSlider->setValue(settings.volumeLevel);

    connect(ui->bookList, &QListWidget::currentRowChanged, this, &MainWindow::activateBook);
    connect(ui->chapterList, &QListWidget::currentRowChanged, this, &MainWindow::activateChapter);
    connect(ui->playBtn, &QPushButton::clicked, this, &MainWindow::play);
    connect(player, &QMediaPlayer::positionChanged, this, &MainWindow::positionChanged);
    connect(player, &QMediaPlayer::mediaStatusChanged, this, &MainWindow::mediaStatusChanged);

    loadState();
}

MainWindow::~MainWindow()
{
    saveState();
    settings.save();
    delete ui;
}

QString MainWindow::getLibraryFilePath() {
    return getAppDataDir().absoluteFilePath(libraryFileName);
}

void MainWindow::on_addBook_btn_clicked()
{
    QStringList filePaths = QFileDialog::getOpenFileNames(this, tr("Choose audio file(s)"), QString(), tr("Audio Files (*.mp4 *.m4b *.mp3)"));
    if (filePaths.size() == 0) {
        return;
    }

    Book book;
    long totalLength = 0; // In milliseconds.
    for (int i = 0; i < filePaths.size(); i++) {
        Chapter chapter;
        TagLib::FileRef f(filePaths[i].toUtf8().constData());

        if (i == 0) { // Set the boot metadata based on the first file.
            book.author = QString(f.tag()->artist().toCString());
            book.title = QString(f.tag()->album().toCString());
        }

        // Tracks seem to start at 1.
        if (f.tag()->track() == 0) { // Track order tag was not set.
            std::cout << "Using iterator (" << i << ") as track order" << std::endl;
            chapter.order = i + 1;
        } else {
            chapter.order = f.tag()->track();
        }
        chapter.title = QString(f.tag()->title().toCString());
        int length = f.audioProperties()->lengthInMilliseconds();
        chapter.length = length;
        totalLength += length;
        chapter.fileName = filePaths[i];

        book.chapters.push_back(chapter);
    }

    book.totalLength = totalLength;
    std::sort(book.chapters.begin(), book.chapters.end(), [](Chapter first, Chapter second) {
        return first.order < second.order;
    });
    books.push_back(book);
    ui->bookList->addItem(book.title);
    qDebug() << "Added book:" << book.title;
    saveState();
}

void MainWindow::saveState() {
    QFile libFile(getLibraryFilePath());
    if (!libFile.open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, tr("Unable to open the library file."), libFile.errorString());
        return;
    }
    QDataStream out(&libFile);
    out.setVersion(QDataStream::Qt_5_11);
    out << activeBookId;
    out << books;
}

void MainWindow::loadState() {
    QFile libFile(getLibraryFilePath());
    if (!libFile.exists()) {
        return;
    }
    if (!libFile.open(QIODevice::ReadOnly)) {
        QMessageBox::information(this, tr("Unable to open the library file."), libFile.errorString());
        return;
    }
    QDataStream in(&libFile);
    in.setVersion(QDataStream::Qt_5_11);
    int activeBook;
    in >> activeBook;
    in >> books;

    // Reload the book list.
    ui->bookList->clear();
    for (auto& book : books) {
        ui->bookList->addItem(book.title);
    }
    if (books.size() > 0) {
        ui->bookList->setCurrentRow(activeBook);
    }
}

void MainWindow::activateBook(int currentRow) {
    if (currentRow == -1) { // No book is selected.
        ui->author->clear();
        ui->bookTitle->clear();
        ui->chapterList->clear();
        ui->removeBook_btn->setEnabled(false);
        return;
    }
    if (currentRow < 0 || currentRow >= books.size()) {
        qDebug() << "Invalid book index:" << currentRow;
        return;
    }

    pause();
    activeBookId = currentRow;
    activeBook = &books[currentRow];

    ui->bookTitle->setText(activeBook->title);
    ui->author->setText(activeBook->author);
    ui->chapterList->clear();
    for (auto& ch : activeBook->chapters) {
        ui->chapterList->addItem(QString::number(ch.order) + " - " + ch.title);
    }
    ui->chapterList->setCurrentRow(activeBook->currentChapter);
    ui->removeBook_btn->setEnabled(true);
}

void MainWindow::activateChapter(int currentRow) {
    if (currentRow == -1) { // No chapter is selected.
        ui->totalTime->setText("00:00");
        ui->timePlayed->setText("00:00");
        ui->progressSlider->setValue(0);
        ui->progressSlider->setEnabled(false);
        ui->nextChapterBtn->setEnabled(false);
        ui->forwardTenBtn->setEnabled(false);
        ui->playBtn->setEnabled(false);
        ui->backTenBtn->setEnabled(false);
        ui->lastChapterBtn->setEnabled(false);
        return;
    }
    if (activeBook == nullptr) {
        qDebug() << "Tried activating a chapter while no book is active.";
        return;
    }
    if (currentRow < 0 || currentRow >= activeBook->chapters.size()) {
        return;
    }

    if (activeBook->currentChapter != currentRow) {
        activeBook->currentChapter = currentRow;
        activeBook->currentPosition = 0;
    }
    setMedia(activeBook->activeChapter().fileName, activeBook->activeChapter().length, activeBook->currentPosition);
    if (isPlaying) {
        player->play();
    }
    ui->progressSlider->setEnabled(true);

    if (activeBook->currentChapter == 0) {
        ui->lastChapterBtn->setEnabled(false);
    } else {
        ui->lastChapterBtn->setEnabled(true);
    }
    if (activeBook->currentChapter == activeBook->chapters.size() - 1) {
        ui->nextChapterBtn->setEnabled(false);
    } else {
        ui->nextChapterBtn->setEnabled(true);
    }
}

void MainWindow::play() {
    player->play();
    isPlaying = true;
    disconnect(ui->playBtn, &QPushButton::clicked, this, &MainWindow::play);
    connect(ui->playBtn, &QPushButton::clicked, this, &MainWindow::pause);
    ui->playBtn->setText(tr("Pause"));
}

void MainWindow::pause() {
    player->pause();
    isPlaying = false;
    disconnect(ui->playBtn, &QPushButton::clicked, this, &MainWindow::pause);
    connect(ui->playBtn, &QPushButton::clicked, this, &MainWindow::play);
    ui->playBtn->setText(tr("Play"));
}

void MainWindow::positionChanged(qint64 position) {
    if (activeBook == nullptr) {
        qDebug() << "Position changed while no book was active.";
        return;
    }
    activeBook->currentPosition = position;
    ui->progressSlider->setValue(position);
    ui->timePlayed->setText(millisecondsToTimespan(position));
}

void MainWindow::mediaStatusChanged(QMediaPlayer::MediaStatus status)
{
    if (status == QMediaPlayer::EndOfMedia) {
        if (activeBook->currentChapter != activeBook->chapters.size() - 1) {
            ui->chapterList->setCurrentRow(activeBook->currentChapter + 1);
        }
    }
}

void MainWindow::setMedia(QString filePath, int length, int position) {
    player->setMedia(QUrl::fromLocalFile(filePath));
    ui->progressSlider->setMaximum(length);
    ui->totalTime->setText(millisecondsToTimespan(length));
    if (position >= 0) {
        player->play();
        player->setPosition(position);
        player->pause();
    }

    ui->backTenBtn->setEnabled(true);
    ui->playBtn->setEnabled(true);
    ui->forwardTenBtn->setEnabled(true);
}

void MainWindow::on_progressSlider_sliderMoved(int position)
{
    player->setPosition(position);
}

void MainWindow::on_volumeSlider_sliderMoved(int position)
{
    player->setVolume(position);
    settings.volumeLevel = position;
}

void MainWindow::on_lastChapterBtn_clicked()
{
    if (activeBook->currentChapter == 0) {
        return;
    }
    ui->chapterList->setCurrentRow(activeBook->currentChapter - 1);
}

void MainWindow::on_nextChapterBtn_clicked()
{
    if (activeBook->currentChapter == activeBook->chapters.size() - 1) {
        return;
    }
    ui->chapterList->setCurrentRow(activeBook->currentChapter + 1);
}

void MainWindow::on_backTenBtn_clicked()
{
    player->setPosition(player->position() - 10000);
}

void MainWindow::on_forwardTenBtn_clicked()
{
    player->setPosition(player->position() + 10000);
}

void MainWindow::on_removeBook_btn_clicked()
{
    QMessageBox msgBox;
    msgBox.setText(tr("Are you sure you want to remove %1 from the library?").arg(activeBook->title));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    int ret = msgBox.exec();

    if (ret == QMessageBox::Yes) {
        player->stop();
        activeBook = nullptr;
        books.removeAt(activeBookId);
        // Reinitialize the books list to avoid weird interactions, when an item is removed.
        ui->bookList->clear();
        for (auto& book : books) {
            ui->bookList->addItem(book.title);
        }
        if (activeBookId < books.size()) {
            ui->bookList->setCurrentRow(activeBookId);
        } else if (activeBookId != 0) {
            ui->bookList->setCurrentRow(activeBookId - 1);
        } else {
            ui->bookList->setCurrentRow(-1);
        }
        saveState();
    }
}
