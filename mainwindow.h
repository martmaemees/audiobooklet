#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMediaPlayer>
#include <QStringListModel>
#include <QList>
#include <QFile>
#include "book.h"
#include "settings.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    /**
     * @brief Adds a book to the library.
     *
     * Displays a file open dialog that allows multiple files.
     * Reads IDv3 tags from all files and adds them into the library as a single book.
     */
    void on_addBook_btn_clicked();
    void on_progressSlider_sliderMoved(int position);
    void on_volumeSlider_sliderMoved(int position);
    void on_lastChapterBtn_clicked();
    void on_nextChapterBtn_clicked();
    void on_backTenBtn_clicked();
    void on_forwardTenBtn_clicked();

    /**
     * @brief Called when a new row in the book list is selected.
     *
     * Populates the detail view with the book metadata and chapter list.
     * Selects the chapter in the chapter list based on book state.
     *
     * If nothing is selected in the list, depopulates the detail view.
     */
    void activateBook(int currentRow);

    /**
     * @brief Called when a new row in the chapter list is selected.
     *
     * Loads the audio file into the MediaPlayer and seeks to
     * the position saved in the book state.
     */
    void activateChapter(int currentRow);
    void play();
    void pause();
    void positionChanged(qint64 position);
    void mediaStatusChanged(QMediaPlayer::MediaStatus status);
    void setMedia(QString filePath, int length, int position = -1);

    void on_removeBook_btn_clicked();

private:
    Ui::MainWindow* ui;

    QMediaPlayer* player = new QMediaPlayer;
    QStringListModel* bookListModel;

    Book* activeBook = nullptr;
    int activeBookId;
    QList<Book> books;
    bool isPlaying = false;
    Settings settings;

    void saveState();
    void loadState();
    QString getLibraryFilePath();
};
#endif // MAINWINDOW_H
