#include "settings.h"
#include "util.h"

#include <QMessageBox>
#include <QDebug>

const QString settingsFileName = "settings.bin";

Settings Settings::load()
{
    Settings res;
    QFile file(getAppDataDir().filePath(settingsFileName));
    if (!file.open(QIODevice::ReadOnly)) {
        return res;
    }
    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_5_11);
    in >> res.volumeLevel;
    return res;
}

void Settings::save()
{
    QFile file(getAppDataDir().filePath(settingsFileName));
    if (!file.open(QIODevice::WriteOnly)) {
        qDebug() << "Unable to open the settings file for write.";
        return;
    }
    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_5_11);
    out << volumeLevel;
}

QDataStream& operator<<(QDataStream& out, const Settings& settings)
{
    out << settings.volumeLevel;
    return out;
}

QDataStream& operator>>(QDataStream& in, Settings& settings)
{
    in >> settings.volumeLevel;
    return in;
}
